if [ -d "$HOME/.oh-my-zsh" ]; then
  echo -e "\nOh My Zsh is already installed\n"
else
  echo -e  "\nInstalling oh-my-zsh\n"
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

# Remove backup config
rm /home/vincent/.zshrc.pre-oh-my-zsh 2> /dev/null

echo -e  "Installing plugins\n"

# https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md
rm -r -f ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting 2> /dev/null
git clone https://github.com/zsh-users/zsh-syntax-highlighting ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
rm -r -f ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions 2> /dev/null
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# https://github.com/unixorn/fzf-zsh-plugin
rm -r -f ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/fzf-zsh-plugin 2> /dev/null
git clone https://github.com/unixorn/fzf-zsh-plugin.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/fzf-zsh-plugin

# Install powerline fonts
git clone https://github.com/powerline/fonts.git --depth=1
./fonts/install.sh
rm -rf fonts

# Install spaceship-zsh
# https://github.com/denysdovhan/spaceship-prompt
rm -r -f ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/spaceship-prompt 2> /dev/null
rm -f ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/spaceship.zsh-theme 2> /dev/null

git clone https://github.com/denysdovhan/spaceship-prompt.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/spaceship-prompt
ln -s ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/spaceship-prompt/spaceship.zsh-theme ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/spaceship.zsh-theme
