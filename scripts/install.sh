#!/bin/bash
set -e

# Install via APT
# NOTES:
# - fzf is installed as zsh plugin, not here
echo -e "\n====================="
echo -e "=> Installing via apt\n"
sudo apt -qq update && sudo apt install -y \
    git git-lfs htop ssh vim nmap snapd curl wget gcc zsh bat ripgrep terminator tree neofetch cmake clang gimp vlc ffmpeg tmux pdfgrep sl cowsay gparted \
    libfuse2 \
    flatpak \
    fonts-firacode gnome-tweaks redshift-gtk \
    pipx

pipx ensurepath
pipx install poetry
pipx install pre-commit

# docker -> seperate process, google it

# Uninstall
sudo apt remove -y \
    thunderbird

# Cleanup
sudo apt -qq upgrade -y
sudo apt -qq autoremove -y
sudo apt -qq autoclean -y

echo -e "\n======================"
echo -e "=> Installing via snap\n"
sudo snap install slack
sudo snap install spotify
sudo snap install telegram-desktop
sudo snap install better-mouse-speed
sudo snap install dbeaver-ce

# Flatpack extension manager
PACKAGE='com.mattjakeman.ExtensionManager'
echo -e "\n======================"
echo -e "=> Installing $PACKAGE"
if flatpak list | grep -q "$PACKAGE"; then
    echo -e "=> $PACKAGE is already installed."
else
    echo -e "=> Installing $PACKAGE..."

    sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    sudo flatpak install flathub com.mattjakeman.ExtensionManager
    echo "[Desktop Entry]
    Name=Gnome Extensions Flatpak
    GenericName=Gnome Extensions Flatpak
    Comment=Gnome Extensions Flatpak
    Exec='flatpak run com.mattjakeman.ExtensionManager'
    Terminal=false
    Type=Application
    Categories=Settings;" > ~/.local/share/applications/gnome-extensions.desktop
fi

# Displaylink driver
PACKAGE='displaylink-driver'
echo -e "\n======================"
echo -e "=> Installing $PACKAGE"
if dpkg -l | grep -q "^ii  $PACKAGE"; then
    echo -e "=> $PACKAGE is already installed."
else
    echo -e "=> Installing $PACKAGE..."

    wget https://www.synaptics.com/sites/default/files/Ubuntu/pool/stable/main/all/synaptics-repository-keyring.deb
    sudo dpkg -i synaptics-repository-keyring.deb
    rm synaptics-repository-keyring.de*
    sudo apt update
    sudo apt install -y displaylink-driver
fi

# Visual Studio Code
PACKAGE='visual studio code'
echo -e "\n======================"
echo -e "=> Installing $PACKAGE"
if command -v code &> /dev/null
then
	echo -e "=> $PACKAGE is already installed."
else
	echo -e "=> Installing $PACKAGE..."
    curl -L https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64 -o code.deb
    sudo apt install ./code.deb
    rm code.de*
fi

# PYENV
PACKAGE='pyenv'
echo -e "\n======================"
echo -e "=> Installing $PACKAGE"
if [[ -d "$HOME/.pyenv" ]]; then
	echo -e "=> $PACKAGE is already installed."
else
	echo -e "=> Installing $PACKAGE..."
	sudo apt update
	sudo apt install -y build-essential libssl-dev zlib1g-dev \
	libbz2-dev libreadline-dev libsqlite3-dev curl git \
	libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

	curl https://pyenv.run | bash

	# Install recent python version
	pyenv install 3.12.0
	pyenv global 3.12.0
fi


echo -e "\n==================================="
echo -e "=> Finished installing and updating\n"
