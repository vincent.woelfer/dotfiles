#!/bin/bash

# Disable ubuntu dock for >= 20.04
gnome-extensions disable ubuntu-dock@ubuntu.com

# Remove Desktop dir
sed -i 's/XDG_DESKTOP_DIR=.*/XDG_DESKTOP_DIR="$HOME\/.config\/desktop"/g' ~/.config/user-dirs.dirs
rmdir ~/Desktop 2> /dev/null

# Remove unused directories in home -> NOT used since it breaks mqttt explorer
echo "Do you want to remove Documents/Public/Templates/Videos directories? This will break mqtt explorer!"
select choice in "yes" "no"; do
  case $choice in
    yes)
      echo "Removing dirs..."
      rmdir ~/Documents 2> /dev/null
      rmdir ~/Public 2> /dev/null
      rmdir ~/Templates 2> /dev/null
      rmdir ~/Videos 2> /dev/null

      # Prevent xdg from reverting it
      echo -e "enabled=False\nfilename_encoding=UTF-8" > ~/.config/user-dirs.conf
      break
      ;;
    no)
      echo "NOT removing dirs..."
      break
      ;;
    *)
      echo "Invalid option. Please select again."
      ;;
  esac
done

#
rm -f ~/examples.desktop

# Free up Ctrl+Shift+E key combo
gsettings set org.freedesktop.ibus.panel.emoji hotkey "@as []"

# Set background
echo "Copying backgrounds..."
mkdir -p ~/Pictures/wallpapers
cp ~/dotfiles/other-data/cyberpunk.png ~/Pictures/wallpapers
cp ~/dotfiles/other-data/gestalt_background.png ~/Pictures/wallpapers

# Set alt-tab to only show current workspace windows
gsettings set org.gnome.shell.app-switcher current-workspace-only true

# Touchpad settings
gsettings set org.gnome.desktop.peripherals.touchpad disable-while-typing true
gsettings set org.gnome.desktop.peripherals.touchpad disable-while-typing true
# Important one!
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll false

# Mouse
gsettings set org.gnome.desktop.peripherals.mouse natural-scroll false

# Disable ambient light control
gsettings set org.gnome.settings-daemon.plugins.power ambient-enabled false

# Disable all UI animations
gsettings set org.gnome.desktop.interface enable-animations false

# Copy home-dir configs
echo -e  "\nCopying config files to /home/.../\n"
cp -r -a -v ~/dotfiles/home/. "$HOME"

