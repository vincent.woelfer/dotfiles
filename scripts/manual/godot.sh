#!/bin/bash

mkdir -p ~/.godot
cd ~/.godot

echo -e "\nDownload latest version of Godot!"
echo -e "You need the linux.x86_64 version!"
echo -e "Firefox will open now!\n\n"
sleep 5
firefox https://github.com/godotengine/godot/releases

echo -e "\n\nMove to ~/.godot/ and extract!\n"
cp ~/dotfiles/other-data/godot_icon.png ~/.godot/

echo '[Desktop Entry]
Name=Godot Engine
GenericName=Libre game engine
Comment=Multi-platform 2D and 3D game engine with a feature-rich editor
Exec=/home/vincent/.godot/latest --editor --path "/home/vincent/private/hex/"
Icon=/home/vincent/.godot/godot_icon.png
Terminal=false
PrefersNonDefaultGPU=true
Type=Application
MimeType=application/x-godot-project;
Categories=Development;IDE;
StartupWMClass=Godot' > ~/.local/share/applications/godot.desktop

echo -e '\nPlease create a symlink to the latest godot executable like this:'
echo -e 'ln -sf /home/vincent/.godot/XXXXX /home/vincent/.godot/latest'

