#!/bin/bash

# Gestalt Scripts
echo '\nInstalling Gestalt Scripts'
git clone git@gitlab.gestalt-robotics.com:gestaltrobotics/gestalt_scripts.git ~/gestalt/gestalt_scripts
echo 'export PATH="$HOME/gestalt/gestalt_scripts/:$PATH"' > ~/gestalt/gestalt_scripts/_sourceme.sh

echo '\nInstalling tailscale'
curl -fsSL https://tailscale.com/install.sh | sh
sudo tailscale up --login-server https://headscale.gestalt-robotics.com --accept-routes
