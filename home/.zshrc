# Path to your oh-my-zsh installation.
export ZSH="/home/vincent/.oh-my-zsh"

# ZSH Spaceship config
ZSH_THEME="spaceship"

# This will check for updates every 7 days
zstyle ':omz:update' frequency 7

SPACESHIP_PROMPT_ORDER=(
    # time            # Time stamps section
    # user            # Username section
    dir               # Current directory section
    host              # Hostname section
    git               # Git section (git_branch + git_status)
    package           # Package version
    venv              # virtualenv section
    python            # Pyenv section
    kubectl_context   # kubernetes context
    exec_time         # Execution time
    line_sep          # Line break
    # battery         # Battery level and status
    jobs              # Background jobs indicator
    exit_code         # Exit code section
    char              # Prompt character
)

# Spaceship Theme config
SPACESHIP_GIT_PREFIX=""
SPACESHIP_GIT_STATUS_PREFIX=" "
SPACESHIP_GIT_STATUS_SUFFIX=""

SPACESHIP_EXEC_TIME_PREFIX=""
SPACESHIP_EXEC_TIME_COLOR="white"

SPACESHIP_DIR_TRUNC=6
SPACESHIP_DIR_TRUNC_REPO=true
SPACESHIP_DIR_TRUNC_PREFIX="\u27A4 "

SPACESHIP_KUBECTL_CONTEXT_SHOW=$([[ -n "$KUBECONFIG" ]] && echo true || echo false)
SPACESHIP_KUBECTL_CONTEXT_SHOW=true
SPACESHIP_KUBECTL_CONTEXT_PREFIX="%F{26}⎈%f "
SPACESHIP_KUBECTL_CONTEXT_SHOW_NAMESPACE="false"
SPACESHIP_KUBECTL_CONTEXT_COLOR="26"

# Standard oh-my-zsh config
HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

# History
HIST_STAMPS="dd.mm.yyyy"
HISTSIZE=900000
HISTFILESIZE=$HISTSIZE
SAVEHIST=$HISTSIZE
HISTFILE=$HOME/.zsh_history
setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
#setopt hist_ignore_space     # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt inc_append_history     # add commands to HISTFILE in order of execution
setopt share_history          # share command history data

# autosuggestion
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=240"

# GIT Plugin
# true = disable marking untracked files under VCS as dirty. This makes repository status check for large repositories much faster
DISABLE_UNTRACKED_FILES_DIRTY="true"

#################################################
# ZSH PLUGINS
#################################################
# nvm = node version manager
zstyle ':omz:plugins:nvm' lazy yes

plugins=(
    git
    zsh-syntax-highlighting
    zsh-autosuggestions
    tailscale
    docker
    fzf-zsh-plugin
    colored-man-pages
    colorize
    pyenv
)

source "$ZSH/oh-my-zsh.sh"

#################################################
# NON ZSH SETTINGS
#################################################

# Add gestalt-scripts if present
[ -f ~/gestalt/gestalt_scripts/_sourceme.sh ] && source ~/gestalt/gestalt_scripts/_sourceme.sh

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='vim'
else
    export EDITOR='vim'
fi

### Gestalt Pyenv Config
export PYENV_ROOT="$HOME/.pyenv/"
[[ -d "$PYENV_ROOT/bin" ]] && export PATH="$PYENV_ROOT/bin:$PATH"
[[ -d "$PYENV_ROOT" ]] && eval "$(pyenv init --path)"
[[ -d "$PYENV_ROOT" ]] && eval "$(pyenv init -)"
[[ -d "$PYENV_ROOT" ]] && eval "$(pyenv virtualenv-init -)"

# Completion for kubernetes & helm
command -v kubectl &> /dev/null && source <(kubectl completion zsh)
command -v helmfile &> /dev/null && source <(helmfile completion zsh)
command -v helm &> /dev/null && source <(helm completion zsh)
command -v kind &> /dev/null && source <(kind completion zsh)

# Aliases for kubernetes and kubecolor and completion. Only alias to kubecolor if it exists
if command -v kubecolor &> /dev/null && command -v kubectl &> /dev/null; then
    alias kubectl=kubecolor
    compdef kubecolor=kubectl
fi
alias kd='kubectl describe'
alias k=kubectl

# Append path (previously for poetry but usefull in general i think)
export PATH="/home/vincent/.local/bin:$PATH"

### MISC Aliases ###
alias ll=la
alias update='sudo apt update -y && sudo apt upgrade -y && sudo apt autoremove -y && sudo apt autoclean -y && sudo snap refresh && flatpak update -y'
alias json='python3 -m json.tool'
alias dc='docker compose'

alias bat='batcat --theme=base16'
alias cat='batcat --theme=base16 --paging=never'
alias batdiff='git diff --name-only --relative | xargs batcat --diff --theme=base16'

alias godot='~/.godot/godot'

### GIT Aliases ###
# Only for remote copy, already there by zsh git plugin:
alias gs='git status'
alias gl='git pull'
alias gp='git push'
alias gb='git branch'
alias gco='git checkout'
alias gd='git diff'
alias ga='git add'
alias gaa='git add --all'
alias gc='git commit --verbose'
alias glol='git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset"'
alias glola='git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset" --all'

# Custom Git aliases:
alias gs='git status'
alias gwip='git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit --no-verify --no-gpg-sign -m "-- wip --" && git push -o ci.skip'
alias gsub='git submodule update --init --recursive'

# Custom Git functions:

gt() {
    if [ -z "$1" ]; then
        echo "Error: Tag name is required."
        return 1
    fi
    git tag "$1" && git push origin "$1"
}

git-clean() {
    if test "$#" -ne 1; then
        MASTER_BRANCH=$(git remote show origin | grep "HEAD branch" | sed "s/.*: //")
    else
        MASTER_BRANCH="$1"
    fi
    echo "Using branch: $MASTER_BRANCH"
    git checkout "$MASTER_BRANCH" && git fetch -f --all --tags --prune --prune-tags --jobs=4 && git pull && git branch -r | awk "{print \$1}" | egrep -v -f /dev/fd/0 <(git branch -vv | grep origin) | grep -v "\*" | awk "{print \$1}" | xargs --no-run-if-empty git branch -d
}

git-push-merge() {
    MASTER_BRANCH=$(git remote show origin | grep "HEAD branch" | sed "s/.*: //")
    CURR_BRANCH=$(git symbolic-ref --short -q HEAD)
    [ -z "$CURR_BRANCH" ] && echo "Could not get current branch name!" && return 1
    if [ "$CURR_BRANCH" = "$MASTER_BRANCH" ]; then
        echo "On brach ${MASTER_BRANCH}, please first checkout a new branch locally!"
        return 1
    fi
    git push --set-upstream origin "${CURR_BRANCH}" -o merge_request.create -o merge_request.target="${MASTER_BRANCH}" -o merge_request.merge_when_pipeline_succeeds && git checkout "${MASTER_BRANCH}"
}

### C-MAKE Build Helper script
cmake-build() {
    MODE="Release"
    if [[ "$@" =~ "debug" ]]; then
        MODE="Debug"
    fi
    echo "=> Using config: $MODE"

    mkdir -p build && cmake -B build -G "Unix Makefiles" && cmake --build build/ --config "$MODE" --target all --parallel 7
}

### Convert/Compress Videos for whatsapp
sanitize-video() {
    if test "$#" -lt 1 || test "$#" -gt 2; then
        echo "Please specify input file and optional resolution!"
        return 1
    fi

    filename=$(basename -- "$1")
    extension="mp4"
    filename="${filename%.*}"
    resolution=${2:-1080}  # Defaults to 1080 if not specified
    echo "Using resolution: ${resolution}p"

    ffmpeg -i "$1" -c:v libx264 -pix_fmt yuv420p -vf "scale=-1:${resolution}, pad=ceil(iw/2)*2:ceil(ih/2)*2" -profile:v baseline -level 3.0 -an -movflags +faststart "${filename}_compressed.$extension"
}

record_video() {
    ffmpeg -f x11grab -s 3840x2160 -i :0.0+0,0 -r 30 -c:v libx264 -preset ultrafast -pix_fmt yuv420p Screencast.mp4
}

### Set Mouse speed
set-mouse-speed() {
    if test "$#" -ne 1; then
        echo "Please specify speed in range 0-100!"
	return 1
    fi

    SPEED=$(printf "%.2f" $(echo "scale=2; ($1 / 50) - 1" | bc))
    echo "Setting mouse speed to $1 | $SPEED"
    gsettings set org.gnome.desktop.peripherals.mouse speed $SPEED
}

### SSH-Source with own bashrc ###
#ssh-gestalt() {
#    # Check if shell is specified, default = bash
#    SHELL="bash"
#    for p in "$@"
#    do
#        shift
#        if [ "$p" = "zsh" ]; then
#            SHELL="zsh"
#        elif [ "$p" = "sh" ]; then
#            #SHELL="sh"
#            echo "Sorry, sh shell is currently not supported!"
#        elif [ "$p" = "fish" ]; then
#            SHELL="fish"
#            #echo "Sorry, fish shell is currently not supported!"
#        elif [ "$p" = "bash" ]; then
#            echo "Using bash by default, this parameter is ignored!"
#        else
#            set -- "$@" "$p"
#        fi
#    done
#    GESTALT_SSH_ALIASES=$(alias | sed --regexp-extended "s/(.*)=(.*)/alias -- \1=\2/" | sed "s/nocorrect//" | base64 -w 0)
#    ssh -t $@ "echo \"${GESTALT_SSH_ALIASES}\" | base64 --decode > /tmp/gestalt_ssh_aliases; "${SHELL}" --rcfile <(cat /tmp/gestalt_ssh_aliases; cat ~/."${SHELL}"rc)"
#}
