#!/bin/bash

echo -e "\n### Setup ###\n"

echo -e "Do you want to set the git global email now? [y/N]"
read -r -e -t 60 SET_GIT_EMAIL
if [[ "$SET_GIT_EMAIL" =~ ^(y|Y|yes|Yes)$ ]]; then
  echo "Please enter git email address:"
  read -r GIT_EMAIL
  echo "Setting email to: $GIT_EMAIL"
fi

#echo -e "\nInstalling software\n"
#sudo ./scripts/install.sh

echo -e "\nSetting up oh-my-zsh\n"
./scripts/setup-oh-my-zsh.sh

echo -e "\nConfiguring gnome\n"
./scripts/configure-gnome.sh

echo -e "\nConfiguring git\n"
git config --global user.name "Vincent Wölfer"
if [[ "$SET_GIT_EMAIL" =~ ^(y|Y|yes|Yes)$ ]]; then
  git config --global user.email "$GIT_EMAIL"
fi
git lfs install
git config --global filter.lfs.process
git config --global push.autoSetupRemote true
git config --global fetch.prune true

echo -e "\nIncreasing inotify handles limit\n"
echo fs.inotify.max_user_watches=65536 | sudo tee -a /etc/sysctl.conf
echo fs.inotify.max_user_instances=256 | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

echo -e "\nConfiguring Power-Switch on lid close\n"
echo 'HandleLidSwitch=ignore' | sudo tee -a /etc/systemd/logind.conf

echo -e "\nUsing Timezone from BIOS correctly\b"
timedatectl set-local-rtc 1 --adjust-system-clock


echo -e "\n##########\nDone\n##########\n"



