#1 Run pre-zsh-setup.sh

#2 Run setup.sh

#3 Gnome-Tweaks:
https://flathub.org/apps/com.mattjakeman.ExtensionManager
- Install shell-extensions from list other-data/gnome-shell-extensions

# Firefox:
- about:config:
- setbrowser.uidensity = 1

# Keyboard shortcuts:
Settings -> Keybpard -> Shotcuts -> Windows
Maximize Window = Super+Up
Restore Window = Super+Down

# Clone git with specific ssh key:
GIT_SSH_COMMAND="ssh -i ~/.ssh/vincent_private_gitlab" git clone git@gitlab.com:vincent.woelfer/hexhill.git
