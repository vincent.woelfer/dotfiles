#!/bin/bash
set -e
NAME="$(whoami)"

if [ "${NAME}" == "root" ] ; then
	echo -e  "DONT RUN THIS DIRECTLY AS ROOT!\n"
	exit 1
fi

echo -e  "\n### Pre-ZSH Setup ###\n\n"

echo -e  "Installing software\n"
./scripts/install.sh

echo -e  "\n##########\n"

if [ $SHELL != "/usr/bin/zsh" ] ; then
	sudo -u "${NAME}" chsh -s $(which zsh)
	echo -e  "Made zsh your default shell, please log out and back in to complete!\n"	
else
	echo -e  "Zsh is already your default shell, not doing anything\n"
fi

echo -e  "##########\n"
